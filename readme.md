# Web Scanner
ネットワーク上のWebサイトの情報を収集する

- masscan
- wget
- wkhtmltoimage(スクリーンショット)
- host（IP逆引き）

により情報を収集する

scanner.pyの引数にIPアドレスのリストを渡すことでスキャンを開始する
Linux上での実行を想定しているが、os.systemで呼び出しているコマンドを
揃えることでMac上でも実行可能
