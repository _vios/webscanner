#coding:utf8

"""
wgetのバージョンの注意
"""
import sys
import os

SCANNER = "masscan"
#Linux
#TIMEOUT = "timeout 15"
#Mac 
TIMEOUT = "gtimeout 15"
PORT = "80"

def log(msg,withCommand=False):
    print msg
    if withCommand: os.system(msg)

ipList = sys.argv[1]

for line in open(ipList,"r"):
    ipRange = line.strip()
    ipRangeDir = ipRange.replace("/","s")
    wgetDir = os.path.join(ipRangeDir,"wget")
    wkhtmlDir = os.path.join(ipRangeDir,"wkhtml")
    nameDir = os.path.join(ipRangeDir,"name")
    listenIPList = os.path.join(ipRangeDir,"%stcp" % PORT)
    log("mkdir %s" % (ipRangeDir),withCommand=True)
    log("mkdir %s" % (wgetDir),withCommand=True)
    log("mkdir %s" % (wkhtmlDir),withCommand=True)
    log("mkdir %s" % (nameDir),withCommand=True)
    log("%s -p%s %s > %s" % (SCANNER,PORT,ipRange,listenIPList),withCommand=True)
    log("cut -d' ' -f6 %s | xargs -P 10 -I {} %s python host.py {} %s/{}" % (listenIPList,TIMEOUT,nameDir),withCommand=True)
    log("cut -d' ' -f6 %s | xargs -P 20 -I {} %s python wget.py {} %s/{} %s" % (listenIPList,TIMEOUT,wgetDir,os.path.join(wgetDir,"log")),withCommand=True)
    log("cut -d' ' -f6 %s | xargs -P 20 -I {} %s python capture.py {} %s/{} %s" % (listenIPList,TIMEOUT,wkhtmlDir,os.path.join(wkhtmlDir,"log")),withCommand=True)

