#wget must be <1.14
import sys
import os

WGET = "wget"
url = sys.argv[1]
saveName = sys.argv[2]
logName = sys.argv[3]
command = '%s -O %s -t 2 --save-headers --content-on-error --header="User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0" --header="Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" --header="Accept-Language: ja,en-US;q=0.7,en;q=0.3" --header="Accept-Encoding: gzip, deflate" --header="Connection: keep-alive" %s >> %s 2>&1' % (WGET,saveName,url,logName)
os.system(command)
sys.exit(0)
